require('env2')('.env');

module.exports = {
  user: process.env.CCNV_EMAIL,
  pwd: process.env.CCNV_PWD,
  from: `"Contato CCNV" <${process.env.CCNV_EMAIL}>`
};
