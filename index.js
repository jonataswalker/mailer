require('env2')('.env');

const nodemailer = require('nodemailer');
const striptags = require('striptags');
const ccnv = require('./clients/ccnv');


module.exports = (sender, to, subject, content) => {
  let from;
  let auth = { user: '', pass: '' };

  console.log(ccnv);

  switch (sender) {
    case 'ccnv':
      from = ccnv.from;
      auth.user = ccnv.user;
      auth.pass = ccnv.pwd;
      break;
    default:
      // ...
  }

  const transporter = nodemailer.createTransport({
    auth,
    host: process.env.UOL_SMTP,
    port: process.env.UOL_SMTP_PORT,
    secure: false // true for 465, false for other ports
  });

  const options = {
    from,
    to,
    subject,
    text: striptags(content)
  };

  return new Promise((resolve, reject) => {
    transporter.sendMail(options, (error, info) => {
      if (error) {
        console.log(error);
        reject(error);
      } else {
        resolve(info);
      }
    });
  });
};
